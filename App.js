import React from 'react';
import BottomNavigation from './src/navigation/BottomNavigation';
import { Provider as PaperProvider } from 'react-native-paper';

const App = () => {
  return (
    <PaperProvider>
      <BottomNavigation />
    </PaperProvider>
  )
}

export default App;
