import React from 'react';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import HomeScreen from '../screens/HomeScreen';
import AddNotificationScreen from '../screens/AddNotificationScreen';
import { NavigationContainer } from '@react-navigation/native';
import Ionicons from 'react-native-vector-icons/Ionicons';

const Tab = createMaterialBottomTabNavigator();

const BottomNavigation = () => {
    
  return (
      <NavigationContainer>
          <Tab.Navigator initialRouteName='Home'>
              <Tab.Screen 
                name="Home" 
                component={HomeScreen}
                options={{
                    tabBarLabel:'Notifications',
                    tabBarIcon: ({ color }) => (
                        <Ionicons name='ios-home-outline' size={25} color={color}/>
                    )
                }
                }/>
              <Tab.Screen 
                name="Add Notification" 
                component={AddNotificationScreen} 
                options={{
                    tabBarLabel:'Ajouter Notifications',
                    tabBarIcon: ({ color }) => (
                        <Ionicons name='add' size={25} color={color}/>
                    )
                }
                }
                />
          </Tab.Navigator>
      </NavigationContainer>
  );
}

export default BottomNavigation;