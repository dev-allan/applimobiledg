import Realm from "realm";
import { NotificationSchema } from "./schemas/NotificationSchema";

export const getRealm = async () => {
try {
    const realm = await Realm.open({
      path: "NotificationDb",
      schema: [NotificationSchema],
      schemaVersion: 1,
    });
    return realm
  } catch (err) {
    console.error("Failed to open the realm", err.message);
  }
}