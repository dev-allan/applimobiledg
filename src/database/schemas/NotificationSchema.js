export const NotificationSchema = {
    name: 'Notification',
    properties: {
      _id: 'objectId',
      title: 'string?',
      theme: 'string?',
      desc: 'string?',
    },
    primaryKey: '_id',
  };