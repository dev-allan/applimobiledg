import React, { useState, useEffect } from 'react';
import { View, Text, FlatList, SafeAreaView, StyleSheet, StatusBar } from 'react-native';

import {getRealm} from '../database/getRealmApp';
import { Button } from 'react-native-paper';
import {useIsFocused} from '@react-navigation/native';

const Item = ({ title, theme, desc }) => (
    <View style={styles.item}>
      <Text>Titre : {title}</Text>
      <Text>Thème : {theme}</Text>
      <Text>Description : {desc}</Text>
    </View>
  );


const HomeScreen = () => {
    const [notification, setNotification] = useState([]);
    const [refresh, setRefresh] = useState(false);
    const isFocused = useIsFocused();

    const renderItem = ({ item }) => (
            <Item title={item.title} theme={item.theme} desc={item.desc}/>
      );
    
    const getRealmApp = async () => {
        let realm = await getRealm();
        let thisNotification = realm.objects("Notification");
        return thisNotification;
      };

    const deleteAllNotification = async () => {
        let realm = await getRealm();
        realm.write(() => {
            realm.delete(realm.objects("Notification"));
        })
        setRefresh(true)
    }

      useEffect(() => {
        getRealmApp().then(data => {
          setNotification(data);
        });
        setRefresh(false)
      }, [refresh, isFocused]);
      
    return (
        <SafeAreaView style={styles.container}>
            <Button color={'red'} onPress={() => deleteAllNotification()}>Tout supprimer</Button>
            <FlatList
                data={notification}
                renderItem={renderItem}
                keyExtractor={item => item._id}
            />
        </SafeAreaView>
    );
};

const styles = StyleSheet.create({
    container: {
      flex: 1,
    },
    item: {
      backgroundColor: '#9370db',
      padding: 20,
      marginVertical: 8,
      marginHorizontal: 16,
    },
  });

export default HomeScreen;