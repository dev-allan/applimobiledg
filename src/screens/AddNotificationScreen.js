import React, { useState } from 'react';
import { StyleSheet, SafeAreaView, View } from 'react-native';
import { getRealm } from '../database/getRealmApp';
import 'react-native-get-random-values';
import {ObjectID} from 'bson';

import { TextInput, Button } from 'react-native-paper';

const AddNotificationScreen = () => {
    const [title, setTitle] = useState('');
    const [theme, setTheme] = useState('');
    const [desc, setDesc] = useState('');


    const getRealmApp = async () => {
        let realm = await getRealm();
        return realm;
      };
    
      const addNotification = async () => {
        const id = ObjectID();
        let data = {
          _id: id,
          title : title,
          theme : theme,
          desc : desc,
        };
        let notif;
        let realm = await getRealmApp();
        realm.write(() => {
          notif = realm.create('Notification', data);
        });
      };

    return (
        <SafeAreaView style={styles.container}>
            <View>
                <TextInput
                    label="titre de la notification"
                    value={title}
                    onChangeText={title => setTitle(title)}
                />

                <TextInput
                    label="theme de la notification"
                    value={theme}
                    onChangeText={theme => setTheme(theme)}
                />

                <TextInput
                    label="description de la notification"
                    value={desc}
                    onChangeText={desc => setDesc(desc)}
                />
                <Button mode="contained" onPress={() => addNotification()}>
                    Envoyer
                </Button>
            </View>
        </SafeAreaView>
    );
};

const styles = StyleSheet.create({
    container: {
      flex: 1,
    },
  });

export default AddNotificationScreen;